# CryptoTask Nuxt frontend

## Development

```bash
# pull from git
$ git pull origin dev

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev
```

## Production

### First time setup

```bash
# pull from git
$ git pull origin master

# install dependencies (with --production false flag dev dependencies will be installed also)
$ yarn install --production false

# build for production
$ yarn build

# start pm2 processes
$ pm2 start
```

PM2 will start 2 processes called CryptotaskFrontend using ecosystem.config.js file.

### New version deployment

```bash
# deploy
$ yarn deploy
```

This will do all the work for you, pull new version of code, install new dependencies if needed and restart processes.
