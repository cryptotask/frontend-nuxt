export default {
  env: {
    baseUrl: process.env.API_URL,
    tawkSrc: process.env.TAWK_SRC,
    bcAddressStore: process.env.BC_ADDRESS_STORE,
    bcAddressLogic: process.env.BC_ADDRESS_LOGIC,
    bcLogicVersion: process.env.BC_LOGIC_VERSION,
    bcURL: process.env.BC_URL,
    imagesBucket: process.env.PUBLIC_BUCKET,
    imagesURL: process.env.IMAGE_BASE_URL,
    bucketURL: process.env.BUCKET_URL,
    filesizeLimit: process.env.FILESIZE_LIMIT,
    gtagEn: process.env.GTAG_EN,
    gtagHr: process.env.GTAG_HR,
    gtagEvent: process.env.GTAG_EVENT,
    googleTagManagerEn: process.env.GOOGLE_TAG_EN,
    googleTagManagerHr: process.env.GOOGLE_TAG_HR,
    domainLanguage: process.env.DOMAIN_LANGUAGE,
    ctaskAddress: process.env.CTASK_ADDRESS,
    tezosUrl: process.env.TEZOS_URL,
    tezosNetworkId: process.env.TEZOS_NETWORK_ID,
    tezosContract: process.env.TEZOS_CONTRACT,
    tezosReadApi: process.env.TEZOS_READ_API,
    fbPixel: process.env.FB_PIXEL,
    hCaptcha: process.env.HCAPTCHA_SITE_KEY,
    captchaEnabled: process.env.CAPTCHA_ENABLED
  },

  publicRuntimeConfig: {
    gtagEn: process.env.GTAG_EN,
    gtagHr: process.env.GTAG_HR,
    googleTagManagerEn: process.env.GOOGLE_TAG_EN,
    googleTagManagerHr: process.env.GOOGLE_TAG_HR
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        property: 'og:type',
        content: 'website',
        hid: 'og:type'
      },
      {
        name: 'twitter:card',
        content: 'summary_large_image',
        hid: 'twitter:card'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/style.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/AxiosLanguage',
    '~plugins/Vuelidate',
    '~plugins/userLocale',
    {
      src: '~plugins/Tawk',
      mode: 'client'
    },
    {
      src: '~plugins/SmartContract',
      mode: 'client'
    },
    {
      src: '~plugins/VueSocketIo',
      mode: 'client'
    },
    {
      src: '~plugins/StarRating',
      mode: 'client'
    },
    {
      src: '~plugins/Datepicker',
      mode: 'client'
    },
    {
      src: '~plugins/VueCropper',
      mode: 'client'
    },
    {
      src: '~plugins/MomentLocale',
      mode: 'client'
    },
    '~plugins/Jsonld',
    /* {
      src: '~plugins/Gtm',
      mode: 'client'
    }, */
    {
      src: '~plugins/Gtag',
      mode: 'client'
    },
    {
      src: '~plugins/FbPixel',
      mode: 'client'
    },
    {
      src: '~plugins/DateFormatFilter',
      mode: 'client'
    },
    {
      src: '~plugins/Vuescroll',
      mode: 'client'
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/fontawesome'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-basic-auth-module',
    '@nuxtjs/sentry',
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'nuxt-i18n',
    '@nuxtjs/moment',
    'nuxt-precompress',
    ['nuxt-lazy-load', {
      defaultImage: '/images/default-image.png',
      directiveOnly: true
    }],
    // '@nuxtjs/gtm',
    ['nuxt-facebook-pixel-module', {
      /* module options */
      pixelId: 'undefined', // will be defined later on in plugin
      track: 'PageView',
      autoPageView: true,
      disabled: true
    }]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    devtools: process.env.NODE_ENV !== 'production',
    extend (config, {
      isDev,
      isClient
    }) {
      if (isDev) {
        config.devtool = isClient ? 'eval-source-map' : 'inline-source-map'
      }
    },
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }]
      ]
    }
  },

  auth: {
    redirect: {
      login: '/login',
      home: '/',
      logout: '/login'
    },
    strategies: {
      local: {
        token: {
          property: 'data.token'
        },
        user: {
          autoFetch: false,
          property: 'data'
        },
        endpoints: {
          login: {
            url: '/auth/login',
            method: 'post'
          },
          user: {
            url: '/users/me',
            method: 'get'
          },
          logout: false
        }
      }
    },
    plugins: [
      '~/plugins/AuthRedirect.js'
    ]
  },

  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en-US.js',
        name: 'English',
        iso: 'en-US'
      },
      {
        code: 'hr',
        file: 'hr-HR.js',
        name: 'Hrvatski',
        iso: 'hr-HR'
      }
    ],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'en',
    strategy: 'prefix',
    seo: false,
    vuex: {
      syncLocale: true
    },
    baseUrl: 'https://www.cryptotask.org'
  },

  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false // Or `bvCSS: false`
  },

  router: {
    middleware: ['auth']
  },

  // moment config
  moment: {
    defaultLocale: 'en',
    locales: ['hr']
  },

  /* gtm: {
    enabled: process.env.NODE_ENV === 'production'
  }, */

  sentry: {
    config: {
      environment: process.env.NODE_ENV,
      denyUrls: [
        /va\.tawk\.to/i,
        /tzkt\.io/i
      ]
    },
    clientIntegrations: {
      ReportingObserver: false
    }
  },

  basic: {
    name: process.env.BASIC_NAME,
    pass: process.env.BASIC_PASSWORD,
    enabled: process.env.BASIC_ENABLED === 'true'
  },

  fontawesome: {
    icons: {
      brands: ['faMediumM', 'faTwitter', 'faYoutube', 'faTelegramPlane', 'faDiscord', 'faRedditAlien', 'faLinkedinIn', 'faFacebookF', 'faInstagram', 'faTiktok']
    }
  },

  vue: {
    config: {
      devtools: process.env.NODE_ENV !== 'production'
    }
  }
}
