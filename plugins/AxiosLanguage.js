// noinspection JSUnusedGlobalSymbols
export default function ({
  $axios,
  i18n
}) {
  $axios.onRequest(() => {
    $axios.setHeader('x-language', i18n.locale)
  })
}
