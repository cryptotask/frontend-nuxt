import Vue from 'vue'
import VueCropper from 'vue-cropperjs'
import 'cropperjs/dist/cropper.css'
// eslint-disable-next-line vue/component-definition-name-casing
Vue.component('vue-cropper', VueCropper)
