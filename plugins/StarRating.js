import Vue from 'vue'
import StarRating from 'vue-star-rating'

// eslint-disable-next-line vue/component-definition-name-casing
Vue.component('star-rating', StarRating)
