import capitalize from 'lodash/capitalize'

export default function ({
  $gtm,
  i18n,
  env,
  isDev
}) {
  if (!isDev) {
    const { locale } = i18n
    $gtm.init(env[`googleTagManager${capitalize(locale)}`])
  }
}
