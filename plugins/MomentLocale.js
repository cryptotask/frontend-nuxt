export default (context) => {
  const routePath = context.route.path.split('/')
  context.$moment.locale(routePath[1])
}
