export default function ({ $auth, localePath }) {
  $auth.onRedirect((to) => {
    return localePath(to)
  })
}
