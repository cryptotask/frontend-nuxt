import Vue from 'vue'
import Tawk from 'vue-tawk'

Vue.use(Tawk, {
  tawkSrc: process.env.tawkSrc
})
