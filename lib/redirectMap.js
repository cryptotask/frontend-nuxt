export default {
  client: '/my-tasks',
  freelancer: '/tasks',
  agent: '/agents/tasks'
}
