import { helpers } from 'vuelidate/lib/validators'
import * as sanitizeHtml from 'sanitize-html'

export default param => helpers.withParams({
  type: 'richTextMaxCharacters',
  max: param
}, (value) => {
  if (value) {
    const stripped = sanitizeHtml(value, {
      allowedTags: []
    })
    return stripped.length <= param
  }

  return true
})
