export default function ({
  store,
  redirect,
  $auth,
  localePath
}) {
  if ($auth.loggedIn) {
    const isAgent = store.getters['user/isAgent']
    if (!isAgent) {
      store.dispatch('ui/showNotification', {
        type: 'danger',
        text: 'Please log in as agent'
      })

      // redirect user to tasks list
      return redirect(localePath('/tasks'))
    }

    return true
  }

  return true
}
