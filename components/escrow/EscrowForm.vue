<template>
  <div class="position-relative">
    <b-overlay :show="loadingApplications">
      <div v-if="!hasEscrow">
        <h6>{{ $t('escrow.step1intro') }}</h6>
        <p>{{ $t('escrow.step1text') }}</p>

        {{ $t('escrow.step2selectCurrency') }}:
        <b-form @submit.prevent="selectCurrency">
          <div class="mb-3">
            <b-form-select
              v-model="selectedCurrencyInternal"
              :options="currencies"
              size="md"
              class="mt-3"
              :disabled="!enabled"
            />
          </div>
          <b-alert variant="warning" :show="isEth">
            {{ $t('escrow.ethWarning') }}
          </b-alert>
          <div class="d-flex align-items-center justify-content-end">
            <slot name="buttons" />
            <b-button
              variant="primary"
              type="submit"
              class="btn-round"
              :disabled="!selectedCurrencyInternal || !enabled"
            >
              {{ $t('escrow.buttonLabel') }}
            </b-button>
          </div>
        </b-form>
      </div>

      <div v-if="hasEscrow && task.escrowPublic">
        <div>{{ $t('escrow.step3deposit') }}: <strong>{{ task.escrowPublic }}</strong></div>
        <div class="text-center my-3" v-html="task.qrCode" />
        <div>
          <div>{{ $t('escrow.step4balance') }}:</div>
          <div v-if="!loadingBalance" class="d-flex align-items-center">
            <escrow-balance class="mr-3" :task="task" />
            <b-button
              v-if="canWithdraw"
              variant="danger"
              size="sm"
              @click.prevent="withdrawModalVisible = !withdrawModalVisible"
            >
              {{ $t('escrow.withdraw') }}
            </b-button>
            <div v-if="isWithdrawal">
              ({{ $t('escrow.withdrawal') }})
            </div>
            <div v-if="isWithdrawn">
              ({{ $t('escrow.withdrawn') }})
            </div>
          </div>
          <div v-if="loadingBalance">
            <strong>{{ $t('common.loading') }}</strong>
          </div>
          <p>{{ $t('escrow.step4depositMessage') }}</p>
        </div>
        <slot name="enabled" />
      </div>
    </b-overlay>

    <b-modal
      id="withdrawal-modal"
      v-model="withdrawModalVisible"
      :title="$t('escrow.withdrawalSettings')"
      :ok-disabled="withdrawing"
      @ok.prevent="setForWithdrawal"
    >
      <input-group
        v-model="withdrawForm.address"
        :label="$t('escrow.enterWithdrawalAddress')"
        :description="$t('escrow.withdrawalAddressHelp')"
        :validation="$v.withdrawForm.address"
      />
    </b-modal>
  </div>
</template>

<script>
import AxiosErrorHandler from '@/mixins/AxiosErrorHandler'
import EscrowBalance from '@/components/escrow/EscrowBalance'
import { applicationStatuses, escrowStatuses, taskStatuses } from 'ct-constants'
import InputGroup from '@/components/form/InputGroup'
import { alphaNum, required } from 'vuelidate/lib/validators'

export default {
  name: 'EscrowForm',
  components: {
    InputGroup,
    EscrowBalance
  },
  mixins: [AxiosErrorHandler],
  props: {
    task: {
      type: Object,
      required: true
    }
  },
  data () {
    return {
      isVisible: false,
      selectedCurrencyInternal: null,
      escrowBalance: null,
      loadingBalance: false,
      withdrawing: false,
      withdrawn: false,
      loadingApplications: false,
      withdrawModalVisible: false,
      withdrawForm: {
        address: null
      }
    }
  },
  validations: {
    withdrawForm: {
      address: {
        required,
        alphaNum
      }
    }
  },
  computed: {
    /**
     * Check if escrow can be added based on task status
     * @returns {boolean}
     */
    enabled () {
      return [taskStatuses.CREATED, taskStatuses.ACCEPTING].includes(this.task.status)
    },

    /**
     * Check if task has escrow applied
     * @returns {boolean}
     */
    hasEscrow () {
      return this.task.escrowStatus > escrowStatuses.NO_ESCROW
    },

    /**
     * Get array of curencies for select input
     * @returns {array}
     */
    currencies () {
      return this.$store.state.util.cryptos.map(c => ({
        value: c.name,
        text: c.name
      }))
    },
    /**
     * Checks if current user is logged in
     * @returns {boolean}
     */
    isLoggedIn () {
      return this.$auth.loggedIn
    },

    applications () {
      return this.$store.getters['clientApplications/taskApplications']
    },

    /**
     * Check how many non-created applications does current task have
     * @returns {boolean}
     */
    hasActiveApplications () {
      return !!(this.task && this.applications.find(a => a.status > applicationStatuses.CREATED))
    },

    /**
     * Can escrow be withdrawn
     * @returns {boolean}
     */
    canWithdraw () {
      return this.task?.escrowStatus === escrowStatuses.ESCROW_SET &&
        this.task?.escrowBalance > 0 &&
        !this.hasActiveApplications && !this.withdrawn
    },

    /**
     * Is escrow withdrawn
     * @returns {boolean}
     */
    isWithdrawn () {
      return this.task?.escrowStatus === escrowStatuses.ESCROW_WITHDRAWN
    },

    /**
     * Is secrow set for withdrawal
     * @returns {boolean}
     */
    isWithdrawal () {
      return this.task?.escrowStatus === escrowStatuses.ESCROW_WITHDRAWAL || this.withdrawn
    },

    isEth () {
      return this.$store.state.util.cryptos.find(c => c.name === this.selectedCurrencyInternal)?.bc === 'ETH'
    }
  },
  mounted () {
    this.loadApplications()
  },
  methods: {
    /**
     * Load task applications
     * @returns {Promise<void>}
     */
    async loadApplications () {
      try {
        this.loadingApplications = true
        await this.$store.dispatch('clientApplications/fetchApplications', this.task?.id)
        this.loadingApplications = false
      } catch (err) {
        this.loadingApplications = false
        await this.handleError(err)
      }
    },

    /**
     * Handler for withdraw button
     * @returns {Promise<void>}
     */
    async setForWithdrawal () {
      this.$v.withdrawForm.$touch()
      if (!this.$v.withdrawForm.$invalid) {
        this.withdrawing = true
        try {
          await this.$axios.put(`/tasks/${this.task.id}/escrow-withdraw`, this.withdrawForm)
          await this.$store.dispatch('ui/showNotification', {
            type: 'success',
            text: this.$t('escrow.withdrawSuccess')
          })
          this.withdrawing = false
          this.withdrawn = true
          this.withdrawModalVisible = false
        } catch (err) {
          this.withdrawing = false
          await this.handleError(err)
        }
      }
    },

    /**
     * Select escrow currency handler
     * @returns {Promise<void>}
     */
    async selectCurrency () {
      if (this.selectedCurrencyInternal) {
        try {
          await this.$store.dispatch('tasks/setTaskEscrow', this.selectedCurrencyInternal)
          this.$emit('currency')
        } catch (err) {
          await this.handleError(err)
        }
      }
    }
  }
}
</script>

<style scoped>

</style>
