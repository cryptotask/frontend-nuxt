export default {
  data () {
    return {
      items: [],
      loading: false,
      perPage: 10,
      page: 1,
      total: 0
    }
  },
  created () {
    this.getData()
  },
  methods: {
    changePage (page) {
      this.page = page
      this.getData()
    }
  }
}
