import { applicationClientFlag, applicationStatuses, escrowStatuses, taskStatuses } from 'ct-constants'

export default {
  props: {
    application: {
      type: Object,
      required: true
    },
    task: {
      type: Object,
      required: true
    },
    hiring: {
      type: Boolean,
      default: false
    },
    settingFlag: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    /**
     * Is hire button visible
     * @returns {Boolean}
     */
    hireVisible () {
      return this.application.status === applicationStatuses.CREATED && this.task.status !== taskStatuses.CLOSED
    },

    /**
     * Checks if some other application on current task is already hired
     * @returns {Boolean}
     */
    alreadyHired () {
      return !!this.$store.getters['clientApplications/taskApplicationHires'].length
    },

    /**
     * Is hire button disabled
     * @returns {Boolean}
     */
    hireDisabled () {
      return ![escrowStatuses.NO_ESCROW, escrowStatuses.ESCROW_SET, escrowStatuses.ESCROW_WITHDRAWN, escrowStatuses.ESCROW_WITHDRAWAL].includes(this.task.escrowStatus) ||
        (this.task?.escrowStatus > escrowStatuses.NO_ESCROW && this.alreadyHired)
    },

    /**
     * Can application be completed
     * @returns {Boolean}
     */
    completeEnabled () {
      return this.application.status === applicationStatuses.ACCEPTED
    },

    /**
     * Freelancer canceled the job so client can leave feedback
     * @returns {Boolean}
     */
    leaveFeedbackVisible () {
      return this.application.status === applicationStatuses.CANCELED &&
        this.application.feedback &&
        !this.application.feedback.clientCreatedAt
    },

    /**
     * Check if feedback deadline passed
     * If freelancer added feedback 3 days ago client can not set feedback any more
     * @returns {boolean}
     */
    feedbackDeadlinePassed () {
      if (this.application?.feedback?.freelancerCreatedAt && !this.application?.feedback?.clientCreatedAt) {
        const createdAt = this.$moment(this.application?.feedback?.freelancerCreatedAt)
        const visibleDate = this.$moment().subtract(3, 'days')
        return createdAt.isBefore(visibleDate)
      }
      return false
    },

    /**
     * Is feedback deadline tooltip disabled
     * @returns {boolean}
     */
    feedbackTooltipDisabled () {
      return !this.feedbackDeadlinePassed
    },

    /**
     * Hire confirmation params
     * @returns {{name: *, title: *}}
     */
    confirmationParams () {
      return {
        name: this.application?.freelancer?.name,
        title: this.task?.title
      }
    },

    /**
     * Is application hidden
     * @returns {boolean}
     */
    hidden () {
      return this.application.clientFlag === applicationClientFlag.HIDDEN
    },

    /**
     * Is application starred
     * @returns {boolean}
     */
    starred () {
      return this.application.clientFlag === applicationClientFlag.STARRED
    }
  },
  methods: {
    /**
     * Clicked on hire button
     */
    async hire () {
      const confirm = await this.$bvModal
        .msgBoxConfirm(this.$t('applications.hireConfirmationText', this.confirmationParams), {
          title: this.$t('applications.hireModalTitle'),
          okTitle: this.$t('applications.hireModalButtonLabel'),
          cancelTitle: this.$t('common.cancel')
        })

      if (confirm) {
        this.$emit('hire')
      }
    },

    /**
     * Leave feedback on application
     */
    leaveFeedback (status) {
      this.$emit('feedback', status)
    },

    /**
     * Toggle application visibility status
     */
    async toggleVisibility () {
      if (this.application.clientFlag !== applicationClientFlag.HIDDEN) {
        const confirm = await this.$bvModal
          .msgBoxConfirm(this.$t('applications.hideConfirmationText'))

        if (confirm) {
          this.$emit('toggleFlag', applicationClientFlag.HIDDEN)
        }
      } else {
        this.$emit('toggleFlag', applicationClientFlag.DEFAULT)
      }
    },

    /**
     * Toggle application start
     */
    toggleStar () {
      this.$emit('toggleFlag', this.application.clientFlag === applicationClientFlag.STARRED ? applicationClientFlag.DEFAULT : applicationClientFlag.STARRED)
    }
  }
}
