export default {
  data () {
    return {
      freelancer: null
    }
  },
  async asyncData ({
    error,
    $axios,
    params,
    i18n
  }) {
    const { id } = params
    try {
      const freelancer = await $axios.get(`/freelancers/${id}`)
      return { freelancer: freelancer.data.data }
    } catch (e) {
      if (e.response?.status === 404) {
        error({
          statusCode: 404,
          message: i18n.t('freelancers.notFound')
        })
      }
    }
  }
}
