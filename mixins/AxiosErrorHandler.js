import handleError from '@/lib/handleError'

export default {
  methods: {
    async handleError (error) {
      await handleError(error, this.$i18n, this.$store)
    }
  }
}
