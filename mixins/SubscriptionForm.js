export default {
  data () {
    return {
      loading: false
    }
  },
  async mounted () {
    this.loading = true
    await this.$store.dispatch('subscriptions/fetchPackages')
    this.loading = false
  },
  methods: {
    handleCreatedSubscription (subscription) {
      this.$emit('created', subscription)
    },
    handlePaidSubscription (data) {
      this.$emit('paid', data)
    },
    handlePaidSubscriptionClosed () {
      this.$emit('closed')
    }
  }
}
