export default {
  data () {
    return {
      agent: null
    }
  },
  async asyncData ({
    error,
    $axios,
    params,
    i18n
  }) {
    const { id } = params
    try {
      const res = await $axios.get(`/agents/${id}`)
      return { agent: res.data.data }
    } catch (e) {
      if (e.response?.status === 404) {
        error({
          statusCode: 404,
          message: e.response?.data?.error ? i18n.t(e.response?.data?.error) : i18n.t('404.not_found')
        })
      }
    }
  }
}
