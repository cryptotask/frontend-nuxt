export default {
  computed: {
    task () {
      return this.$store.getters['tasks/selectedTask']
    }
  },
  async fetch () {
    const id = parseInt(this.$route.params.id, 10)
    await this.$store.dispatch('tasks/loadTask', id)
    this.$store.commit('tasks/setSelectedTask', id)
    await this.$store.dispatch('tasks/loadTaskApplicationsCount', id)
  },
  destroyed () {
    this.$store.commit('tasks/deselectTask')
  }
}
