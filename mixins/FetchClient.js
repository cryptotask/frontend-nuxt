export default {
  data () {
    return {
      client: null
    }
  },
  async asyncData ({
    error,
    $axios,
    params,
    i18n
  }) {
    const { id } = params
    try {
      const res = await $axios.get(`/clients/${id}`)
      return { client: res.data.data }
    } catch (e) {
      if (e.response?.status === 404) {
        error({
          statusCode: 404,
          message: i18n.t('clients.notFound')
        })
      }
    }
  }
}
